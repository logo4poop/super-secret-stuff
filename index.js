const express = require('express')
const app = express()
let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}
const fs = require('fs');
var json = fs.readFileSync('./hosting.json', 'utf8');
app.set('json spaces', 40);

console.log(json)
app.get('/hosting.json', (req, res) => res.send(json))

app.listen(port, () => console.log(`Listening on port ${port}!`))
